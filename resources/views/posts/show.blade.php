@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{ $post->title }}</h2>
			<p class="card-subtitle text-muted">Author: {{ $post->user->name }}</p>
			<p class="card-subtitle text-muted">Created at: {{ $post->created_at }}</p>
			<p class="card-text">{{ $post->content }}</p>

			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{ $post->id }}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains('user_id', Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
			@endif
 
			@if(Auth::id() != $post->user_id)
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">Post a Comment</button>

				</form class="d-inline" method="POST" action="/posts/{{ $post->id }}/comment">
					@method('PUT')
					@csrf
						<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="PostAComment" aria-hidden="true">
						  	<div class="modal-dialog">
						    	<div class="modal-content">
						      		<div class="modal-header">
						        		<h1 class="modal-title fs-5" id="PostAComment">Post a Comment</h1>
						    		</div>
								    <div class="modal-body">
								      	<form>
								      	  <div class="mb-3">
								      	    <label for="message-text" class="col-form-label">Message:</label>
								      	    <textarea class="form-control" id="comment"></textarea>
								      	  </div>
								      	</form>
								      	<button type="button" class="btn btn-primary">Post Comment</button>
								    </div>
						   		</div>
						  	</div>
						</div>
				</form>
			@endif


			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

@endsection
