@extends('layouts.app');

@section('content')
	<form method="POST" action="/posts/{{$post->id}}">
		@method('PUT')
		@csrf

		<div class="form-group">
			<label for="title">Title:</label>
			<input type="text" value="{{$post->title}}" class="form-control" id="title" name="title">
		</div>

		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" id="content" name="content" row="3">{{$post->content}}</textarea>
		</div>

		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Update Post</button>
		</div>


	</form>

@endsection