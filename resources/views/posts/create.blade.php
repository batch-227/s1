@extends('layouts.app');

@section('content')
	<form method="POST" action="/posts">
		
		{{-- detects whether there is a token and in order the page reload not to expire --}}
		@csrf

		<div class="form-group">
			<label for="title">Title:</label>
			<input type="text" class="form-control" id="title" name="title">
		</div>

		<div class="form-group">
			<label for="content">Content:</label>
			<textarea class="form-control" id="content" name="content" row="3"></textarea>
		</div>

		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Create Post</button>
		</div>


	</form>

@endsection